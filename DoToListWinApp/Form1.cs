﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DoToListWinApp
{
    public partial class Form1 : Form
    {
        private int _currentViewType;
        private string _apiURL;
        public Form1()
        {
            InitializeComponent();

            _apiURL = "http://localhost:60945/api/todolist";
            _currentViewType = 1;
            dgvToDoList.AutoGenerateColumns = false;
            lblError.Visible = false;

            setForCurrentViewType();
        }

        private void btnCurrent_Click(object sender, EventArgs e)
        {
            _currentViewType = 1;
            
            setForCurrentViewType();
        }

        private void btnPast_Click(object sender, EventArgs e)
        {
            _currentViewType = 2;

            setForCurrentViewType();
        }

        private void btnAddItem_Click(object sender, EventArgs e)
        {
            lblError.Visible = false;
            if (txtItemDesc.Text.Trim() != string.Empty)
            {
                addToListItems(txtItemDesc.Text.Trim());

                txtItemDesc.Text = string.Empty;
                getDoToListItems();
            }
            else
            {
                lblError.Visible = true;
            }
        }

        private void dgvToDoList_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            bool isValid = false;
            switch (dgvToDoList.Columns[e.ColumnIndex].Name)
            {
                case "Complete":
                    isValid = updateToDoListItems(dgvToDoList.Rows[e.RowIndex].Cells["Id"].Value, false, true);
                    break;
                case "Delete":
                    isValid = updateToDoListItems(dgvToDoList.Rows[e.RowIndex].Cells["Id"].Value, true, false);
                    break;
            }

            if (isValid)
            {
                getDoToListItems();
            }
        }

        private void setForCurrentViewType()
        {
            switch (_currentViewType)
            {
                case 1:
                    btnCurrent.ForeColor = Color.Black;
                    btnCurrent.Font = new Font(btnPast.Font.Name, btnPast.Font.Size, FontStyle.Bold);
                    btnCurrent.BackColor = Color.PaleGreen;

                    btnPast.ForeColor = Color.Black;
                    btnPast.Font = new Font(btnPast.Font.Name, btnPast.Font.Size, FontStyle.Regular);
                    btnPast.BackColor = Color.Gray;

                    dgvToDoList.Columns["Delete"].Visible = true;
                    dgvToDoList.Columns["Complete"].Visible = true;
                    dgvToDoList.Columns["Status"].Visible = false;
                    break;
                case 2:
                    btnPast.ForeColor = Color.Black;
                    btnPast.Font = new Font(btnPast.Font.Name, btnPast.Font.Size, FontStyle.Bold);
                    btnPast.BackColor = Color.PaleGreen;

                    btnCurrent.ForeColor = Color.Black;
                    btnCurrent.Font = new Font(btnPast.Font.Name, btnPast.Font.Size, FontStyle.Regular);
                    btnCurrent.BackColor = Color.Gray;

                    dgvToDoList.Columns["Delete"].Visible = false;
                    dgvToDoList.Columns["Complete"].Visible = false;
                    dgvToDoList.Columns["Status"].Visible = true;
                    break;
            }

            getDoToListItems();
        }

        #region middle layer
        private void getDoToListItems()
        {
            string result = getAPI(_apiURL + "?viewtype=" + _currentViewType);

            List<DoToListItemsDTO> list = JsonConvert.DeserializeObject<List<DoToListItemsDTO>>(result);
            if (list != null && list.Count > 0)
            {
                dgvToDoList.DataSource = list;
            }
            else
            {
                dgvToDoList.DataSource = null;
            }
        }

        private void addToListItems(string description)
        {
            DoToListItemsDTO newItem = new DoToListItemsDTO();
            newItem.Description = description;
            string inputData = JsonConvert.SerializeObject(newItem);

            postAPI(_apiURL, inputData);
        }

        private bool updateToDoListItems(object id, bool isDelete, bool isComplete)
        {
            DoToListItemsDTO newItem = new DoToListItemsDTO();
            newItem.Id = Convert.ToInt32(id);
            newItem.IsCompleted = isComplete;
            newItem.IsDeleted = isDelete;
            string inputData = JsonConvert.SerializeObject(newItem);

            putAPI(_apiURL + "/" + id.ToString(), inputData);

            return true;
        }
        #endregion

        #region API call
        private string getAPI(string url)
        {
            HttpWebRequest webrequest = (HttpWebRequest)WebRequest.Create(url);
            webrequest.Method = "GET";
            webrequest.ContentType = "application/json";

            WebResponse webresponse = webrequest.GetResponse();
            string result = new StreamReader(webresponse.GetResponseStream()).ReadToEnd();

            return result;
        }

        private string postAPI(string url, string inputData)
        {
            HttpWebRequest webrequest = (HttpWebRequest)WebRequest.Create(url);
            webrequest.Method = "POST";
            webrequest.ContentType = "application/json";

            Stream posData = webrequest.GetRequestStream();
            StreamWriter strWrite = new StreamWriter(posData);
            strWrite.Write(inputData);
            strWrite.Close();

            WebResponse webresponse = webrequest.GetResponse();
            string result = new StreamReader(webresponse.GetResponseStream()).ReadToEnd();

            return result;
        }

        private string putAPI(string url, string inputData)
        {
            HttpWebRequest webrequest = (HttpWebRequest)WebRequest.Create(url);
            webrequest.Method = "PUT";
            webrequest.ContentType = "application/json";

            Stream posData = webrequest.GetRequestStream();
            StreamWriter strWrite = new StreamWriter(posData);
            strWrite.Write(inputData);
            strWrite.Close();

            WebResponse webresponse = webrequest.GetResponse();
            string result = new StreamReader(webresponse.GetResponseStream()).ReadToEnd();

            return result;
        }

        #endregion
    }
}
