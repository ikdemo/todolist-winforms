﻿namespace DoToListWinApp
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.btnCurrent = new System.Windows.Forms.Button();
            this.btnPast = new System.Windows.Forms.Button();
            this.dgvToDoList = new System.Windows.Forms.DataGridView();
            this.txtItemDesc = new System.Windows.Forms.TextBox();
            this.btnAddItem = new System.Windows.Forms.Button();
            this.lblError = new System.Windows.Forms.Label();
            this.No = new System.Windows.Forms.DataGridViewImageColumn();
            this.Desc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Status = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Complete = new System.Windows.Forms.DataGridViewButtonColumn();
            this.Delete = new System.Windows.Forms.DataGridViewButtonColumn();
            this.Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgvToDoList)).BeginInit();
            this.SuspendLayout();
            // 
            // btnCurrent
            // 
            this.btnCurrent.BackColor = System.Drawing.Color.PaleGreen;
            this.btnCurrent.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCurrent.Location = new System.Drawing.Point(54, 48);
            this.btnCurrent.Name = "btnCurrent";
            this.btnCurrent.Size = new System.Drawing.Size(129, 37);
            this.btnCurrent.TabIndex = 0;
            this.btnCurrent.Text = "Current";
            this.btnCurrent.UseVisualStyleBackColor = false;
            this.btnCurrent.Click += new System.EventHandler(this.btnCurrent_Click);
            // 
            // btnPast
            // 
            this.btnPast.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.btnPast.Location = new System.Drawing.Point(189, 48);
            this.btnPast.Name = "btnPast";
            this.btnPast.Size = new System.Drawing.Size(129, 37);
            this.btnPast.TabIndex = 1;
            this.btnPast.Text = "Past";
            this.btnPast.UseVisualStyleBackColor = true;
            this.btnPast.Click += new System.EventHandler(this.btnPast_Click);
            // 
            // dgvToDoList
            // 
            this.dgvToDoList.AllowUserToAddRows = false;
            this.dgvToDoList.AllowUserToDeleteRows = false;
            this.dgvToDoList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvToDoList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.No,
            this.Desc,
            this.Status,
            this.Complete,
            this.Delete,
            this.Id});
            this.dgvToDoList.Location = new System.Drawing.Point(54, 109);
            this.dgvToDoList.Name = "dgvToDoList";
            this.dgvToDoList.ReadOnly = true;
            this.dgvToDoList.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvToDoList.Size = new System.Drawing.Size(686, 229);
            this.dgvToDoList.TabIndex = 2;
            this.dgvToDoList.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvToDoList_CellClick);
            // 
            // txtItemDesc
            // 
            this.txtItemDesc.Location = new System.Drawing.Point(54, 362);
            this.txtItemDesc.Multiline = true;
            this.txtItemDesc.Name = "txtItemDesc";
            this.txtItemDesc.Size = new System.Drawing.Size(537, 37);
            this.txtItemDesc.TabIndex = 4;
            // 
            // btnAddItem
            // 
            this.btnAddItem.BackColor = System.Drawing.Color.PaleGreen;
            this.btnAddItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddItem.Location = new System.Drawing.Point(606, 362);
            this.btnAddItem.Name = "btnAddItem";
            this.btnAddItem.Size = new System.Drawing.Size(134, 37);
            this.btnAddItem.TabIndex = 5;
            this.btnAddItem.Text = "Add";
            this.btnAddItem.UseVisualStyleBackColor = false;
            this.btnAddItem.Click += new System.EventHandler(this.btnAddItem_Click);
            // 
            // lblError
            // 
            this.lblError.AutoSize = true;
            this.lblError.ForeColor = System.Drawing.Color.Red;
            this.lblError.Location = new System.Drawing.Point(54, 406);
            this.lblError.Name = "lblError";
            this.lblError.Size = new System.Drawing.Size(149, 13);
            this.lblError.TabIndex = 6;
            this.lblError.Text = "* Please enter any Description";
            // 
            // No
            // 
            this.No.HeaderText = "No";
            this.No.Image = ((System.Drawing.Image)(resources.GetObject("No.Image")));
            this.No.Name = "No";
            this.No.ReadOnly = true;
            // 
            // Desc
            // 
            this.Desc.DataPropertyName = "Description";
            this.Desc.HeaderText = "Description";
            this.Desc.Name = "Desc";
            this.Desc.ReadOnly = true;
            this.Desc.Width = 320;
            // 
            // Status
            // 
            this.Status.DataPropertyName = "StatusText";
            this.Status.HeaderText = "Status";
            this.Status.Name = "Status";
            this.Status.ReadOnly = true;
            // 
            // Complete
            // 
            this.Complete.HeaderText = "Complete";
            this.Complete.Name = "Complete";
            this.Complete.ReadOnly = true;
            this.Complete.Text = "Complete";
            this.Complete.UseColumnTextForButtonValue = true;
            // 
            // Delete
            // 
            this.Delete.HeaderText = "Delete";
            this.Delete.Name = "Delete";
            this.Delete.ReadOnly = true;
            this.Delete.Text = "Delete";
            this.Delete.UseColumnTextForButtonValue = true;
            // 
            // Id
            // 
            this.Id.DataPropertyName = "Id";
            this.Id.HeaderText = "Id";
            this.Id.Name = "Id";
            this.Id.ReadOnly = true;
            this.Id.Visible = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.lblError);
            this.Controls.Add(this.btnAddItem);
            this.Controls.Add(this.txtItemDesc);
            this.Controls.Add(this.dgvToDoList);
            this.Controls.Add(this.btnPast);
            this.Controls.Add(this.btnCurrent);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.dgvToDoList)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCurrent;
        private System.Windows.Forms.Button btnPast;
        private System.Windows.Forms.DataGridView dgvToDoList;
        private System.Windows.Forms.TextBox txtItemDesc;
        private System.Windows.Forms.Button btnAddItem;
        private System.Windows.Forms.Label lblError;
        private System.Windows.Forms.DataGridViewImageColumn No;
        private System.Windows.Forms.DataGridViewTextBoxColumn Desc;
        private System.Windows.Forms.DataGridViewTextBoxColumn Status;
        private System.Windows.Forms.DataGridViewButtonColumn Complete;
        private System.Windows.Forms.DataGridViewButtonColumn Delete;
        private System.Windows.Forms.DataGridViewTextBoxColumn Id;
    }
}

